<?php 

use App\Models\Category;
use Illuminate\Support\Facades\Mail;
use App\Models\Celebrity;


function getCelebrityProfileImage($celebId)
{
    $celebrity = Celebrity::where(['id' => $celebId])->first();
    if(!empty($celebrity)){
        if($celebrity->profile_type == 'local'){
            if($celebrity['profile_pic']){
             $url = url('public/images/profile/' . $celebrity['profile_pic']);
            }
            else{
             $url = url('public/images/user_default.png');
            }
        }else{
            $url = env('IMAGE_BASE_URL'). $celebrity['profile_pic'];
        }
    }
    return $url;
}
