<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;



class Celebrity extends Model
{
   
    protected $table = 'celebrity';

    protected $fillable = [
        'celebname','celebrity_type', 'profile_pic','birthday','person_id','biography','place_of_birth','known_for_department','gender','popularity','adult','imdb_id','known_as'
    ];

    public function celebrityImage()
    {
        return $this->hasMany('App\Models\CelebrityImage', 'celeb_id', 'id');
    }
   
    
   
}
