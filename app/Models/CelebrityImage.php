<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CelebrityImage extends Model
{
   
    protected $table = 'celebrity_image';

    protected $fillable = [
        'id','celeb_id', 'image','image_url'
    ];

    
   
}
