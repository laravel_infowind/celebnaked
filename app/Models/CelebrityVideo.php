<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class CelebrityVideo extends Model
{
   
    protected $table = 'celebrity_video';

    protected $fillable = [
        'id','celeb_id', 'video','video_url','title'
    ];

    
   
}
