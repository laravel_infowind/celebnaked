<?php

namespace App\Providers;


use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Hash ;
use Auth;
// use App\User;
// use App\Models\UserPaymentMethod;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('password_hash_check', function($attribute, $value, $parameters, $validator) {
            print_r(Auth::user());die;
            $user = Auth::user()->password;
            return Hash::check($value , Auth::user()->password) ;
        });
    }
}
