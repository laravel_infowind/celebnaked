@extends('admin::layouts.inner-master')
@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Celebrity Detail</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('admin/celebrity')}}">Celebrity</a></li>
          <li class="breadcrumb-item active">Detail</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
  <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title"><strong>Name: </strong>{{$celebrity->celebname}}</h3>
          </div> 
          <div class="card-body">
            <p><strong>Profile</strong></p>
            <div>
              <img src="{{getCelebrityProfileImage($celebrity->id)}}" width="60px" height="60px">
            </div>
          </div>
          <div class="card-header">
            <h3 class="card-title"><strong>Celebrity type: </strong>{{strtoupper($celebrity->celebrity_type)}}</h3>
          </div> 
          <div class="card-header">
            <h3 class="card-title"><strong>Birthday: </strong>{{$celebrity->birthday}}</h3>
          </div> 
          <div class="card-header">
            <h3 class="card-title"><strong>Birth Place: </strong>{{$celebrity->place_of_birth}}</h3>
          </div>
          <div class="card-header">
            <h3 class="card-title"><strong>Department: </strong>{{$celebrity->known_for_department}}</h3>
          </div>
          <div class="card-header">
            <h3 class="card-title"><strong>Gender: </strong>{{($celebrity->gender==1)?'Female':'Male'}}</h3>
          </div>
          <div class="card-header">
            <h3 class="card-title"><strong>Popularity: </strong>{{$celebrity->popularity}}</h3>
          </div>
          <div class="card-header">
            <h3 class="card-title"><strong>Also Know As: </strong>{{$celebrity->known_as}}</h3>
          </div>
          <div class="card-header">
            <h3 class="card-title"><strong>Biography: </strong>{{$celebrity->biography}}</h3>
          </div> 
        </div>
  </div>
</section>
</div>


@endsection