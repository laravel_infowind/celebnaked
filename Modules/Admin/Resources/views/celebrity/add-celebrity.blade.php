@extends('admin::layouts.inner-master')
@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Add Celebrity</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('admin/celebrity')}}">Celebrity</a></li>
          <li class="breadcrumb-item active">Add</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <form id="add_celebrity" method="post" enctype='multipart/form-data'>
      @csrf
      <div class="row">
        <div class="col-md-8">
          <div class="card card-primary">
            <div class="card-body">
              <input type="hidden" name="profile_pic" id="celeb_profile_pic">
              <input type="hidden" name="gender" id="celeb_gender">
              <input type="hidden" name="popularity" id="celeb_popularity">
              <input type="hidden" name="adult" id="celeb_adult">
              <input type="hidden" name="imdb_id" id="celeb_imdb_id">
              <input type="hidden" name="person_id" id="celeb_person_id">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="form-check">
                      <input id="tmdb_radio" value="tmdb" onclick="checkType('tmdb')" class="form-check-input" type="radio" name="celebrity_type" checked>
                      <label class="form-check-label">TMDB</label>
                    </div>
                    <div class="form-check">
                      <input id="custom_radio" value="custom" onclick="checkType('custom')" class="form-check-input" type="radio" name="celebrity_type">
                      <label class="form-check-label">Custom</label>
                    </div>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="form-group">
                    <label>Celebrity Name</label>
                    <input type="text" name="celebname" id="celebname" required class="form-control require" placeholder="Celebrity Name">
                    <span></span>
                  </div>
                </div>
                <div class="col-sm-2" id="tmdb_btn">
                  <div class="form-group">
                    <label>TMDB API</label>
                    <button type="button" id="seacrh_tmdb" onclick="getCelebrityList()" class="form-control btn btn-block btn-info">Search TMDB</button>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Biography</label>
                    <textarea name="biography" id="celeb_biography" class="form-control" rows="10"></textarea>
                  </div>
                  <span id="error_bio" style="display: none;" class="help-block error-help-block alert-danger">The Biography is required.</span>
                </div>
              </div>

              <div class="form-group">
                <label class="col-form-label" for="inputSuccess"> Birthday</label>
                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                  <input type="text" id="leceb_birthday" name="birthday" data-target="#reservationdate" class="require form-control datetimepicker-input" placeholder="Birthday">
                  <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
                <span id="birthday_error" style="display: none;" class="help-block error-help-block alert-danger">The field is required.</span>
              </div>
              <div class="form-group">
                <label class="col-form-label" for="inputWarning">Birth Place</label>
                <input type="text" id="celeb_place_of_birth" name="place_of_birth" class="require form-control" placeholder="Place birth">
                <span></span>
              </div>
              <div class="form-group">
                <label class="col-form-label" for="inputError">Department</label>
                <input type="text" id="known_for_department" name="known_for_department" class="require form-control" placeholder="Department">
                <span></span>
              </div>
              <div class="form-group">
                <label class="col-form-label" for="inputError">Also Known For</label>
                <input type="text" id="known_as" name="known_as" class="form-control" placeholder="Known for">
                <span></span>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-form-label" for="inputError">Gender</label>
                    <div class="form-check">
                      <input id="male_radio" value="2" class="form-check-input" type="radio" name="gender">
                      <label class="form-check-label">Male</label>
                    </div>
                    <div class="form-check">
                      <input id="female_radio" value="1" class="form-check-input" type="radio" name="gender">
                      <label class="form-check-label">Female</label>
                    </div>
                    <span id="gender_error" style="display: none;" class="help-block error-help-block alert-danger">The gender field is required.</span>
                  </div>
                </div>
              </div>

              <div class="row justify-content-end">
                <div class="col-sm-6">
                  <button type="button" class="btn btn-primary btn-block add_celebrity">Save<i id="add_celebrity_loader" style="display: none;" class="fas fa-sync-alt fa-spin"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <a href="#" class="btn btn-primary btn-block"><b>Upload Image</b></a>
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="{{url('public/images/user_default.png')}}" alt="User profile picture">
              </div>
              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <input id="profile" name="profile" type="file">
                </li>
              </ul>
              <span id="error_profile" style="display: none;" class="help-block error-help-block alert-danger">The Profile is required.</span>
            </div>
          </div>
    </form>
  </div>
  </div>
  </div>
  </div>
</section>
</div>

<div class="modal fade" id="modal-lg">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Actress list</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Thumbnail</th>
              <th>Celebrity Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody id="celebrity-data">

          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <div class="btn-group justify-content-end">
          <button class="tmdb_btn_prev pagination_btn btn btn-primary" onclick="getCelebrityList('prev')" prev_page="0">Prev</button>
          <button class="tmdb_btn_next pagination_btn btn btn-primary" onclick="getCelebrityList('next')" next_page="2">Next</button>
          <button type="button" class="pagination_btn btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

</div>

<script>
  var page = 1;
  var total_page;
  var TMDB_KEY = "{{env('TMDB_API_KEY')}}";

  $(function() {
    $('#celeb_biography').summernote({
      height: 200,
    })
    $('#reservationdate').datetimepicker({
      format: 'YYYY-MM-DD'
    });
  })

  function checkType(type) {
    if (type == 'custom') {
      $('#tmdb_btn').hide();
    } else if (type == 'tmdb') {
      $('#tmdb_btn').show();
    }
  }

  function getCelebrityList(type) {
    if (!$('#celebname').val()) {
      $('#celebname').closest('span').addClass('help-block error-help-block alert-danger').html('The field is required.');
    }

    if (type == 'prev') {
      page = page - 1;
    } else if (type == 'next') {
      page = page + 1;
    }
    var CELEB_NAME = $('#celebname').val();
    if (CELEB_NAME) {
      $('#seacrh_tmdb').prop('disabled', true);
      $('#seacrh_tmdb').text('Searching...');
      $.ajax({
        type: 'get',
        url: "https://api.themoviedb.org/3/search/person?api_key=" + TMDB_KEY + "&query=" + CELEB_NAME + "&page=" + page,
        dataType: 'json',
        success: function(data) {
          total_page = data.total_pages;
          if (page == 1) {
            $('.tmdb_btn_prev').prop('disabled', true);
          } else {
            $('.tmdb_btn_prev').prop('disabled', false);
          }
          if (page == total_page) {
            $('.tmdb_btn_next').prop('disabled', true);
          } else {
            $('.tmdb_btn_next').prop('disabled', false);
          }
          var str = '';
          if (data.results.length > 0) {
            for (var i = 0; i < data.results.length; i++) {
              if (data.results[i].profile_path) {
                var image = "{{env('IMAGE_BASE_URL')}}" + data.results[i].profile_path;
              } else {
                var image = "{{url('public/images/user_default.png')}}";
              }
              str = str + "<tr><td>" + ((20 * (page - 1)) + (i + 1)) + "</td>" +
                "<td><img class='img-thumbnail' src='" + image + "' width='60' height='60'></td>" +
                "<td>" + data.results[i].name + "</td>" +
                "<td><button type='button' onclick = 'getCelebrityDetail(" + data.results[i].id + ")' id='38646' class='btn get-movie-data btn-sm btn-primary'>Select</button></td></tr>";
            }
          } else {
            str = str + "<tr>" +
              "<td colspan='4'>'No Celebrity Found'</td></tr>";

          }

          $('#celebrity-data').html(str);
          console.log(data)
          $('#modal-lg').modal('show');
          $("#modal-lg").modal({
            backdrop: 'static',
            keyboard: false
          });
          $('#seacrh_tmdb').prop('disabled', false);
          $('#seacrh_tmdb').text('Search');
        }
      });
    }
  }

  function getCelebrityDetail(celebrityId) {
    $('.close').trigger('click');
    $.ajax({
      type: 'get',
      url: "https://api.themoviedb.org/3/person/" + celebrityId + "?api_key=" + TMDB_KEY + "&language=en-US",
      dataType: 'json',
      success: function(data) {
        console.log(data)
        $('#celeb_person_id').val(data.id);
        $('#celebname').val(data.name);
        $('#leceb_birthday').val(data.birthday);
        $('#celeb_place_of_birth').val(data.place_of_birth);
        $('#celeb_biography').summernote("code", data.biography);
        $('#known_for_department').val(data.known_for_department);
        $('#celeb_profile_pic').val(data.profile_path);
        $('#celeb_gender').val(data.gender);
        $('#celeb_popularity').val(data.popularity);
        $('#celeb_imdb_id').val(data.imdb_id);
        $('#celeb_known_as').val(JSON.stringify(data.also_known_as));
        $('#celeb_adult').val(data.adult);
        if (data.gender == 2) {
          $("#male_radio").attr('checked', true);
        } else {
          $("#female_radio").attr('checked', true);
        }
        var i;
        var known_str = '';
        for (i = 0; i < data.also_known_as.length; ++i) {
          known_str = known_str + data.also_known_as[i] + ' , ';
        }
        $('#known_as').val(known_str.substring(0, known_str.length - 1));
        if (data.profile_path) {
          var image = "{{env('IMAGE_BASE_URL')}}" + data.profile_path;
        } else {
          var image = "{{url('public/images/user_default.png')}}";
        }
        $('.box-profile .profile-user-img').attr('src', image);
        $('.error-help-block').hide();

      }
    });
  }

  $("#profile").change(function() {
    readURL(this);
  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('.profile-user-img').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  function removeFile(index) {
    console.log(index);
    delete uploadedFiles[index];
    $('#file_' + index).remove();
  }

  $("#add_celebrity .add_celebrity").on('click', function(e) {
    e.preventDefault();
    var flag = 0;
    $(".require").each(function() {
      if ($.trim($(this).val()) == "") {
        $(this).next('span').addClass('help-block error-help-block alert-danger').html('The field is required.');
        flag++;
      } else {
        $(this).next('span').hide();
      }
    });
    var biography = $('#celeb_biography').val();
    if (!biography) {
      $('#error_bio').show();
    } else {
      $('#error_bio').hide();
    }
    if (!$('#leceb_birthday').val()) {
      $('#birthday_error').show();
    } else {
      $('#birthday_error').hide();
    }
    if (!$('input[name="gender"]:checked').val()) {
      $('#gender_error').show();
    } else {
      $('#gender_error').hide();
    }

    var image = true;
    if (!$('#celeb_profile_pic').val() && document.getElementById("profile").files.length == 0) {
      $('#error_profile').show();
      image = false;
    } else {
      $('#error_profile').hide();
    }

    if (!flag && biography && image) {
      var formdata = new FormData($('#add_celebrity')[0]);
      formdata.append('biography', biography);
      formdata.append('profile', $('#profile').val());
      $('.add_celebrity').prop('disabled', true);
      $('#add_celebrity_loader').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        async: false,
        type: 'post',
        processData: false,
        contentType: false,
        url: "{{url('admin/add-celebrity')}}",
        data: formdata,
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            toastr.success(data.message);
            $('#add_celebrity')[0].reset();
            setTimeout(function() {
              window.location = "{{url('admin/celebrity')}}"
            }, 1000)
          } else {
            toastr.error(data.message);
          }
          // $('#add_celebrity_loader').hide();
          $('.add_celebrity').prop('disabled', false);
        }
      });
    }
  });
</script>

@endsection