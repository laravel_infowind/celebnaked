@extends('admin::layouts.inner-master')
@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Edit Celebrity</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="{{url('admin/celebrity')}}">Celebrity</a></li>
          <li class="breadcrumb-item active">Add</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <form id="add_celebrity" method="post" enctype='multipart/form-data'>
      @csrf
      <div class="row">
        <div class="col-md-8">
          <div class="card card-primary">
            <div class="card-body">
              <input type="hidden" name="id" value="{{$celebrity->id}}">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <div class="form-check">
                      <input id="tmdb_radio" value="tmdb" class="form-check-input" @if($celebrity->celebrity_type == 'tmdb') checked @endif type="radio" name="celebrity_type">
                      <label class="form-check-label">TMDB</label>
                    </div>
                    <div class="form-check">
                      <input id="custom_radio" value="custom" class="form-check-input" @if($celebrity->celebrity_type == 'custom') checked @endif type="radio" name="celebrity_type">
                      <label class="form-check-label">Custom</label>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Celebrity Name</label>
                    <input type="text" name="celebname" id="celebname" value="{{$celebrity->celebname}}" required class="form-control require" placeholder="Celebrity Name">
                    <span></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label>Biography</label>
                    <textarea name="biography" id="celeb_biography" class="form-control" rows="10">{{$celebrity->biography}}</textarea>
                  </div>
                  <span id="error_bio" style="display: none;" class="help-block error-help-block alert-danger">The Biography is required.</span>
                </div>
              </div>

              <div class="form-group">
                <label class="col-form-label" for="inputSuccess"> Birthday</label>
                <div class="input-group date" id="reservationdate" data-target-input="nearest">
                  <input type="text" value="{{$celebrity->birthday}}" id="leceb_birthday" name="birthday" data-target="#reservationdate" class="require form-control datetimepicker-input" placeholder="Birthday">
                  <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                  </div>
                </div>
                <span></span>
              </div>
              <div class="form-group">
                <label class="col-form-label" for="inputWarning">Birth Place</label>
                <input type="text" value="{{$celebrity->place_of_birth}}" id="celeb_place_of_birth" name="place_of_birth" class="require form-control" placeholder="Place birth">
                <span></span>
              </div>
              <div class="form-group">
                <label class="col-form-label" for="inputError">Department</label>
                <input type="text" id="known_for_department" value="{{$celebrity->known_for_department}}" name="known_for_department" class="require form-control" placeholder="Department">
                <span></span>
              </div>
              <div class="form-group">
                <label class="col-form-label" for="inputError">Also Known For</label>
                <input type="text" id="known_as" value="{{$celebrity->known_as}}" name="known_as" class="form-control" placeholder="Known for">
                <span></span>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="col-form-label" for="inputError">Gender</label>
                    <div class="form-check">
                      <input id="male_radio" value="2" @if($celebrity->gender == 2) checked @endif class="form-check-input" type="radio" name="gender">
                      <label class="form-check-label">Male</label>
                    </div>
                    <div class="form-check">
                      <input id="female_radio" @if($celebrity->gender == 1) checked @endif value="1" class="form-check-input" type="radio" name="gender">
                      <label class="form-check-label">Female</label>
                    </div>
                    <span id="gender_error" style="display: none;" class="help-block error-help-block alert-danger">The gender field is required.</span>
                  </div>
                </div>
              </div>

              <div class="row justify-content-end">
                <div class="col-sm-6">
                  <button type="button" class="btn btn-primary  btn-block add_celebrity">Update<i id="edit_celebrity_loader" style="display: none;" class="fas fa-sync-alt fa-spin"></i></button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <a href="#" class="btn btn-primary btn-block"><b>Upload Image</b></a>
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="{{getCelebrityProfileImage($celebrity->id)}}" alt="User profile picture">
              </div>
              <ul class="list-group list-group-unbordered mb-3">
                <li class="list-group-item">
                  <input id="profile" name="profile" type="file">
                </li>
              </ul>
              <span id="error_profile" style="display: none;" class="help-block error-help-block alert-danger">The Profile is required.</span>
            </div>
          </div>
    </form>
  </div>
  </div>
  </div>
  </div>
</section>
</div>



<script>
  var page = 1;
  var total_page;
  var TMDB_KEY = "{{env('TMDB_API_KEY')}}";

  $(function() {
    $('#celeb_biography').summernote({
      height: 200,
    })
    $('#reservationdate').datetimepicker({
      format: 'YYYY-MM-DD'
    });
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
  })

  function checkType(type) {
    if (type == 'custom') {
      $('#tmdb_btn').hide();
    } else if (type == 'tmdb') {
      $('#tmdb_btn').show();
    }
  }

  $("#profile").change(function() {
    readURL(this);
  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('.profile-user-img').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  function removeFile(index) {
    console.log(index);
    delete uploadedFiles[index];
    $('#file_' + index).remove();
  }

  $("#add_celebrity .add_celebrity").on('click', function(e) {
    e.preventDefault();
    var flag = 0;
    $(".require").each(function() {
      if ($.trim($(this).val()) == "") {
        $(this).next('span').addClass('help-block error-help-block alert-danger').html('The field is required.');
        flag++;
      } else {
        $(this).next('span').hide();
      }
    });
    var biography = $('#celeb_biography').val();
    if (!biography) {
      $('#error_bio').show();
    }
    if (!$('input[name="gender"]:checked').val()) {
      $('#gender_error').show();
    }

    if (!flag && biography) {
      $('#edit_celebrity_loader').show();
      var formdata = new FormData($('#add_celebrity')[0]);
      formdata.append('biography', biography);
      formdata.append('profile', $('#profile').val());
      $('.add_celebrity').prop('disabled', true);
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        async: false,
        type: 'post',
        processData: false,
        contentType: false,
        url: "{{url('admin/update-celebrity')}}",
        data: formdata,
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            toastr.success(data.message);
            $('#add_celebrity')[0].reset();
            setTimeout(function() {
              window.location = "{{url('admin/celebrity')}}"
            }, 1000)
          } else {
            toastr.error(data.message);
          }
          // $('#edit_celebrity_loader').hide();
          $('.add_celebrity').prop('disabled', false);
        }
      });
    }
  });
</script>

@endsection