@extends('admin::layouts.inner-master')
@section('content')

<div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
        
            <h1 class="m-0">Celebrity List</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
           <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{url('admin/celebrity')}}">Celebrity</a></li>
              <li class="breadcrumb-item active">Add</li>
            </ol>
            <a href="{{url('admin/celebrity-add')}}">
            <button type="submit" id="add_celebrity" class="btn btn-primary">Add Celebrity</button>
            </a>
          </div>
        </div>
      </div>
</div>
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
              <form action="{{url('admin/celebrity')}}" post="get">
              <input type="hidden" name="page" value="{{$page}}">
              <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="search" id="search" class="form-control float-right" placeholder="Search">
                    <input type="hidden" name="page" value="" >
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                      <button type="submit" class="btn btn-default refresh">
                        <i class="fas fa-redo"></i>
                      </button>
                    </div>
                  </div>
              </div>
              </form>
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Celebrity</th>
                      <th>Profile</th>
                      <th>Birthday</th>
                      <th>Celibrity From</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  $i= 1;
                  ?>
                  @if(count($celebrity) > 0)
                  @foreach($celebrity as $celeb)
                  @php
                  $c= ($page-1)*6+$i;
                  $i++;
                  @endphp
                    <tr id="row_{{$celeb->id}}">
                      <td>{{$c}}</td>
                      <td>{{$celeb->celebname}}</td>
                      <td><img src="{{getCelebrityProfileImage($celeb->id)}}" width="60" height="60"></td>
                      <td>{{$celeb->birthday}}</td>
                      <td>{{strtoupper($celeb->celebrity_type)}}</td>
                      <td>
                        <div class="celebrity-action">
                        <a href="{{url('admin/celebrity/'.base64_encode($celeb->id))}}"> <i class="nav-icon fas fa-eye"></i></a>
                         <a href="{{url('admin/edit-celebrity/'.base64_encode($celeb->id))}}"> <i class="nav-icon fas fa-pencil-alt"></i></a>
                         <a href="javascript:void(0)" onclick="deleteCelebrity('{{$celeb->id}}')"><i class="nav-icon fas fa-trash"></i></a>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                  @else
                  <tr>
                      <td colspan="6">No Record Found!</td>
                 </tr>
                  @endif
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <!-- <ul class="pagination pagination-sm m-0 float-right"> -->
                 {{$celebrity->links()}}
                  <!-- <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">&raquo;</a></li> -->
                <!-- </ul> -->
              </div>
            </div>
          </div>
          </div>
          </div>
          <script>
            $('.refresh').on('keyup',function(){
                $('.btn-default').trigger('click');
            });
          </script>
    </section>
   <script>

     
  function deleteCelebrity(id){
    
    if(confirm("Are you sure you want to delete?")){
      $.ajax({
        type: "get",
        url: "{{url('admin/delete-celebrity/')}}/"+id,
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            toastr.success(data.message);
            $('#row_'+id).remove();
          } else {
            toastr.error(data.message);
          }
        }
      });
    }
  }
  </script>
    
@endsection