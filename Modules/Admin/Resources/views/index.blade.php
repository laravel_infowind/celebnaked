<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Celebnaked | Log in </title>
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <link rel="stylesheet" href="{{url('public/admin/plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="{{url('public/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{url('public/css/toastr.css')}}">
  <link rel="stylesheet" href="{{url('public/admin/custom_admin_style.css')}}">
  <link rel="stylesheet" href="{{url('public/admin/dist/css/adminlte.min.css')}}">
  <script src="{{url('public/js/toastr.js')}}"></script>
  <script>
    var universalSocket;

    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "preventDuplicates": true,
      "onclick": null,
      "showDuration": "100",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "show",
      "hideMethod": "hide"
    };
  </script>
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <a href="javascript:void(0)" class="h1"><b>Admin</b></a>
      </div>
      <div class="card-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form id='login-form' method="post">
          @csrf
          <div class="input-group mb-3">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
            <input type="text" name="email" id="name" class="form-control input_user" value="" placeholder="Email">
          </div>

          <div class="input-group mb-3">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
            <input type="password" id="password" name="password" class="form-control input_pass" value="" placeholder="Password">

          </div>

          <div class="row">
            <!-- <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div> -->
            <!-- /.col -->
            <div class="col-4">
              <button type="submit" class="btn btn-primary login_btn btn-block">Sign In<i id="login_loader" style="display: none;" class="fas fa-sync-alt fa-spin"></i></button>
            </div>
          </div>
        </form>
        {!! JsValidator::formRequest('App\Http\Requests\Admin\AdminLoginPostRequest','#login-form') !!}
      </div>
    </div>

  </div>
  <script>
    $("#login-form .login_btn").on('click', function(e) {
      e.preventDefault();
      if ($('#login_checkbox').is(':checked')) {
        $('#login_checkbox').val(1);
      } else {
        $('#login_checkbox').val(0);
      }
      if ($("#login-form").valid()) {
        $('#login_button').prop('disabled', true);
        $('#login_loader').show();
        $.ajax({
          headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
          },
          type: "POST",
          url: "{{url('admin/login')}}",
          data: $('#login-form').serialize(),
          dataType: 'json',
          success: function(data) {
            if (data.success) {
              toastr.success(data.message);
              window.location.href = "{{url('admin/dashboard')}}";
            } else {
              toastr.error(data.message);
            }
            $('#login_loader').hide();
            $('#login_button').prop('disabled', false);
          }
        });
      }
    });
  </script>
  <!-- /.login-box -->

  <!-- jQuery -->
  <script src="{{url('public/admin/plugins/jquery/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('public/vendor/jsvalidation/js/jsvalidation.js')}}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{url('public/admin/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{url('public/admin/dist/js/adminlte.min.js')}}"></script>
</body>

</html>