@extends('admin::layouts.inner-master')
@section('content')

<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0">Account Setting</h1>
      </div>

    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-tabs">
          <div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-five-tab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="custom-tabs-five-overlay-tab" data-toggle="pill" href="#custom-tabs-five-overlay" role="tab" aria-controls="custom-tabs-five-overlay" aria-selected="true">Update Profile</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="custom-tabs-five-overlay-dark-tab" data-toggle="pill" href="#custom-tabs-five-overlay-dark" role="tab" aria-controls="custom-tabs-five-overlay-dark" aria-selected="false">Upload Image</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="custom-tabs-five-normal-tab" data-toggle="pill" href="#custom-tabs-five-normal" role="tab" aria-controls="custom-tabs-five-normal" aria-selected="false">Change Password</a>
              </li>
            </ul>
          </div>
          <div class="card-body">
            <div class="tab-content" id="custom-tabs-five-tabContent">
              <div class="tab-pane fade show active" id="custom-tabs-five-overlay" role="tabpanel" aria-labelledby="custom-tabs-five-overlay-tab">
                <div class="overlay-wrapper">
                  <div class="row">
                    <div class="col-md-8 ">
                      <div class="card card-primary">
                        <div class="card-header">
                          <h3 class="card-title">Update Profile</h3>
                        </div>
                        <form method="post" autocomplete="off" id="update_profile">
                          <div class="card-body">
                            <div class="form-group">
                              <label for="exampleInputEmail1">Email</label>
                              <input type="email" class="form-control" name="email" value="{{Auth::user()->email}}" placeholder="Email">
                            </div>
                            <div class="form-group">
                              <label for="exampleInputPassword1">Username</label>
                              <input type="text" class="form-control" value="{{Auth::user()->username}}" name="username" placeholder="Username">
                            </div>
                          </div>
                          <div class="card-footer">
                            <button type="button" class="btn btn-primary update_profile">Submit<i id="profile_loader" style="display: none;" class="fas fa-sync-alt fa-spin"></i></button>
                          </div>
                        </form>
                        {!! JsValidator::formRequest('App\Http\Requests\Admin\UpdateProfileRequest','#update_profile') !!}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="custom-tabs-five-overlay-dark" role="tabpanel" aria-labelledby="custom-tabs-five-overlay-dark-tab">
                <div class="overlay-wrapper">
                  <div class="row">
                    <div class="col-md-8">
                      <div class="card card-primary">
                        <div class="card-header">
                          <h3 class="card-title">Upload Image</h3>
                        </div>
                        <form method="post" autocomplete="off" id="upload_image" enctype='multipart/form-data'>
                          <div class="card-body">
                            <div class="form-group">
                              <div id="image_prev">
                              <img id="blah" class="img-circle" width="100px" height="100px" src="{{(!empty(Auth::user()->profile)) ?url('public/images/profile/'.Auth::user()->profile):url('public/images/user_default.png')}}" alt="your image" />
                              </div>
                              <label for="imgInp">Profile</label>
                              <div class="input-group">
                                <div class="custom-file">
                                  <input type="file" name="profile" accept="image/*" class="custom-file-input" id="imgInp">
                                  <label class="custom-file-label" for="imgInp">Choose file</label>
                                </div>
                               
                              </div>
                              <span id="image-error" style="display: none;" class="help-block error-help-block alert-danger">The profile field is required.</span>
                            </div>
                          </div>
                          <div class="card-footer">
                            <button type="button" class="btn btn-primary upload_image">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="custom-tabs-five-normal" role="tabpanel" aria-labelledby="custom-tabs-five-normal-tab">
                <div class="row">
                  <div class="col-md-8">
                    <div class="card card-primary">
                      <div class="card-header">
                        <h3 class="card-title">Change Password</h3>
                      </div>
                      <form method="post" action="{{url('admin/change-password')}}" autocomplete="off" id="change_password">
                        @csrf
                        <div class="card-body">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Current Password</label>
                            <input type="password" name="current_password" class="form-control" placeholder="Current Password">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">New Password</label>
                            <input type="password" name="new_password" class="form-control" placeholder="new Password">
                          </div>
                          <div class="form-group">
                            <label for="exampleInputPassword1">Confirm Password</label>
                            <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
                          </div>
                        </div>
                        <div class="card-footer">
                          <button type="button" class="btn btn-primary change_password">Submit<i id="password_loader" style="display: none;" class="fas fa-sync-alt fa-spin"></i></button>
                        </div>
                      </form>
                      {!! JsValidator::formRequest('App\Http\Requests\Admin\ChangePasswordRequest','#change_password') !!}
                    </div>
                  </div>
                  <div>
                  </div>
                </div>
              </div>
              <!-- /.card -->
            </div>
          </div>
        </div>

      </div>
</section>
<script>
  $('#custom-tabs-five-overlay-tab').on('click', function() {
    $('#custom-tabs-five-overlay-dark').hide();
    $('#custom-tabs-five-normal').hide();
    $('.error-help-block').hide();
    $('#custom-tabs-five-overlay').show();
  });

  $('#custom-tabs-five-overlay-dark-tab').on('click', function() {
    $('#custom-tabs-five-overlay').hide();
    $('#custom-tabs-five-normal').hide();
    $('.error-help-block').hide();
    $('#custom-tabs-five-overlay-dark').show();
  });

  $('#custom-tabs-five-normal-tab').on('click', function() {
    $('#custom-tabs-five-overlay').hide();
    $('#custom-tabs-five-overlay-dark').hide();
    $('.error-help-block').hide();
    $('#custom-tabs-five-normal').show();
  });

  $("#change_password .change_password").on('click', function(e) {
    e.preventDefault();
    if ($("#change_password").valid()) {
      $('.change_password').prop('disabled', true);
      $('#password_loader').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        type: "POST",
        url: "{{url('admin/change-password')}}",
        data: $('#change_password').serialize(),
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            toastr.success(data.message);
          } else {
            toastr.error(data.message);
          }
          $('#password_loader').hide();
          $('.change_password').prop('disabled', false);
        }
      });
    }
  });

  $("#update_profile .update_profile").on('click', function(e) {
    e.preventDefault();
    if ($("#update_profile").valid()) {
      $('.update_profile').prop('disabled', true);
      $('#profile_loader').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        type: "POST",
        url: "{{url('admin/update-profile')}}",
        data: $('#update_profile').serialize(),
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            toastr.success(data.message);
            location.reload();
          } else {
            toastr.error(data.message);
          }
          $('#profile_loader').hide();
          $('.update_profile').prop('disabled', false);
        }
      });
    }
  });

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }
  $("#imgInp").change(function() {
    readURL(this);
  });

  $("#upload_image .upload_image").on('click', function(e) {
    e.preventDefault();
    if ($("#upload_image").valid() && $('#imgInp').val()) {
      var formData = new FormData($('#upload_image')[0])
      $('.upload_image').prop('disabled', true);
      $('#image_loader').show();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        async:false,
        type:'post',
        processData: false,
        contentType: false,
        url: "{{url('admin/upload-image')}}",
        data: formData,
        dataType: 'json',
        success: function(data) {
          if (data.success) {
            toastr.success(data.message);
            location.reload();
          } else {
            toastr.error(data.message);
          }
          $('#image_loader').hide();
          $('.upload_image').prop('disabled', false);
        }
      });
    }else{
      $('#image-error').show();
    }
  });
</script>


@endsection