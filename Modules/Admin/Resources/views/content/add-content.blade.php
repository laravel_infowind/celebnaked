@extends('admin::layouts.inner-master')
@section('content')

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Add Content</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('admin/celebrity')}}">Celebrity</a></li>
                    <li class="breadcrumb-item active">Add</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Celebrity Content</h3>
                    </div>
                    <form method="post" autocomplete="off" id="add_content" enctype='multipart/form-data'>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Select Celebrity</label>
                                <select class="form-control" id="celeb_lists">
                                    <option value="">Select Celebrity</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputFile">Upload Image</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="celeb_image[]" multiple class="custom-file-input" id="exampleInputFile">
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                </div>
                        
                            <div id="image_preview"></div>
                            <span id="cele_profile_error" style="display: none" class="help-block error-help-block alert-danger">The profile field is required.</span>
                            </div>
                            
                             
                            <div class="form-group">
                                <label for="exampleInputFile">Celebrity Video:</label>
                                <input type="file" name="celeb_video"  id="video"/>
                               
                                
                            </div> 
                            <div class="form-group">
                                <label for="description">Description</label>
                                        <textarea id="summernote" class="form-control" rows="10">
                                           </textarea>
                            </div>
                           
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-primary add_content">Submit</button>
                        </div>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</section>
</div>
<script>
  $(function () {
    $('#summernote').summernote()
    //   CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
    //   mode: "htmlmixed",
    //   theme: "monokai"
    // });
    getCelebrityList();
  })
  
</script>
<script>
    function getCelebrityList(){
        $.ajax({
                type: 'get',
                url: "{{url('admin/get-celebrity-list')}}",
                dataType: 'json',
                success: function(data) {
                   $('#celeb_lists').html(data.list); 
                }
            });
    }

    $("#exampleInputFile").change(function() {
        readURL(this);
        $('#cele_profile_error').hide();

    });

    $("#upload_vide").change(function() {
        readVideoURL(this);
    });

    function readURL(input) {
        var files = input.files;
        var count = 0;
        uploadedFiles = [];
        for (var i = 0; i < files.length; i++) {
            uploadedFiles.push(files[i])
            var reader = new FileReader();
            var fileType = files[i].type;
            console.log(files[i].type)
            var fileSize = '';
            var fileName = files[i].name;
            reader.readAsDataURL(files[i]);
            reader.onload = function(e) {
                if (fileType.includes("image")) {
                    var img = "<div id='file_" + count + "' style='float:left;'><img style='width:50px;height:50px;float:left'   src='" + e.target.result + "'/><a title='Delete'  href='javascript:void(0)' ><i onclick='removeFile(`" + count + "`)' class='fa fa-times'></i></a></div>" +

                        "</div>";
                }
                count++;
                $('#image_preview').append(img);
            }
        }
    }

    function readVideoURL(videoInput) {
        var videoFiles = videoInput.files;
        var count = 0;
        uploadedVideoFiles = [];
        for (var i = 0; i < videoFiles.length; i++) {
            uploadedVideoFiles.push(videoFiles[i])
            var reader = new FileReader();
            var fileType = videoFiles[i].type;
            var fileSize = '';
            var fileName = videoFiles[i].name;
            reader.readAsDataURL(videoFiles[i]);
            reader.onload = function(e) {
                    var img = "<div id='file_" + count + "' style='float:left;'><img style='width:50px;height:50px;float:left'   src='" + e.target.result + "'/><a title='Delete'  href='javascript:void(0)' ><i onclick='removeFile(`" + count + "`)' class='fa fa-times'></i></a></div>" +
                        "</div>";
                count++;
                $('#video_preview').append(img);
            }
        }
    }

    function removeFile(index) {
        console.log(index);
        delete uploadedFiles[index];
        $('#file_' + index).remove();
    }
    $("#add_content .add_content").on('click', function(e) {
        e.preventDefault();
        
        // if ($("#add_content").valid()) {
            var formdata = new FormData($('#add_content')[0]);
            for (var i = 0; i < uploadedFiles.length; i++) {
                formdata.append('celeb_image[]', uploadedFiles[i]);
            }
            formdata.append('celeb_id', $('#celeb_lists').val());
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                async: false,
                type: 'post',
                processData: false,
                contentType: false,
                url: "{{url('admin/add-content')}}",
                data: formdata,
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        toastr.success('Content Added Successfully.')
                    } else {
                        toastr.error(data.message);
                    }
                }
            });
        // } 
    });
</script>

@endsection