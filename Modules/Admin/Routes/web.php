<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'admin', 'middleware' => 'guest'], function () {
    Route::get('/login', 'AdminController@index');
    Route::post('/login', 'AdminController@login')->name('admin.login');
	// Route::get('/forget-password-form', 'AdminController@forgotForm');
    // Route::post('/forget-password', 'AdminController@forgetPassword')->name('forget.password');
    // Route::get('/reset-password', 'AdminController@getResetPassword');
    // Route::post('/reset-password', 'AdminController@resetPassword')->name('reset.password');
});
    
Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
    Route::get('/dashboard', 'AdminController@dashboard');
    Route::get('/celebrity', 'CelebrityController@index');
    Route::get('/celebrity-list', 'CelebrityController@celebrityList');
    Route::get('/celebrity-add', 'CelebrityController@getAddForm');
    Route::post('/add-celebrity', 'CelebrityController@addCelebrity');
    Route::get('/celebrity/{id}', 'CelebrityController@celebrityView');
    Route::get('/delete-celebrity/{id}', 'CelebrityController@deleteCelebrity');
    Route::get('/edit-celebrity/{id}', 'CelebrityController@getEditForm');
    Route::post('/update-celebrity', 'CelebrityController@updateCelebrity');

    Route::get('/celebrity-content', 'CelebrityController@celebrityContentList');
    Route::get('/get-celebrity-list', 'CelebrityController@getCelebrityList');
    Route::post('/add-content', 'CelebrityController@addContent');
    
    Route::get('/account', 'AdminController@account');
    Route::post('/change-password', 'AdminController@changePassword');
    Route::post('/update-profile', 'AdminController@updateProfile');
    Route::post('/upload-image', 'AdminController@uploadImage');
    Route::get('/logout', 'AdminController@logout');
    
});

?>