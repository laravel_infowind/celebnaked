<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Requests\Admin\AdminLoginPostRequest;
use App\Http\Requests\Admin\ChangePasswordRequest;
use App\Http\Requests\Admin\UpdateProfileRequest;
use Illuminate\Support\Facades\Auth;
use App\Mail\ForgetPassword;
use Response;
use App\User;
use DataTables;
use Validator;
use Redirect;
use Mail;
use Illuminate\Support\Facades\Hash ;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        return view('admin::index');
    }

    public function dashboard()
    {
        return view('admin::dashboard.index');

    }

    public function login(AdminLoginPostRequest $request)
    {
        
        $user = User::where(['email'=>trim($request->email)])->first();
        $credentials = ['email'=>trim($request->email),'password'=>$request->password];
        $remember_me = $request->get('remember')==1 ? true : false;
        if (!empty($user) && Auth::attempt($credentials, true)) {
            if ($remember_me) {
                setcookie("email", "$request->email", time()+7 * 24 * 60 * 60);  
                setcookie("password", "$request->password", time()+7 * 24 * 60 * 60);
            }else{
                setcookie("email", " ");  
                setcookie("password", " "); 
            }
            return Response::json(['success'=>true,'message' => 'Login successfully.']);
        }
        return Response::json(['success'=>false,'message' => 'Invalid Credential.']);
    }

    public function logout()
    {
        Auth::logout();
        \Session::flush();
        return redirect('/admin/login');
    }

    public function account(Request $request){
        return view('admin::account.index');
    }

    public function user(Request $request){
        return view('admin::user.index');
    }
	
	public function forgotForm(){
		return view('admin::forget-password');
	}
    
    public function getResetPassword(){
        return view('admin::reset-password');
    }

    public function changePassword(ChangePasswordRequest $request){

        try{
            $admin =  User::where(['id'=>Auth::user()->id])->first();
            if ($admin) {
                $admin->password = Hash::make($request->new_password);
                $admin->save();
                return Response::json(['success'=>true,'message' => 'Password changed successfully.']);
            }
        }catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }

    
    public function updateProfile(UpdateProfileRequest $request){
        
        try{
            $admin =  User::where(['id'=>Auth::user()->id])->first();
            if ($admin) {
                $admin->username = $request->username;
                $admin->email = $request->email;
                $admin->save();
                return Response::json(['success'=>true,'message' => 'Profile Updated Successfully.']);
            }
        }catch(\Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }

    public function uploadImage(Request $request){

        $dir = url('public/images/profile_pic');
        if( is_dir($dir) === false )
        {
            mkdir($dir);
        }
        $admin =  User::where(['id'=>Auth::user()->id])->first();
        if ($request->hasFile('profile')) {
            $image = $request->file('profile');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/profile/');
            $image->move($destinationPath, $name);
            $admin->profile = $name;
            $admin->save();
            return Response::json(['success'=>true,'message' => 'Image Uploaded Successfully.']); 
          }
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     *
     *
     */
    public function forgetPassword(ForGotPasswordRequest $request)
    {
       
        try {
            $requestUser = User::where('email', $request->email)->first();
            if (!empty($requestUser) && ($requestUser->userInRole[0]->role->name=='admin')) {
                $otp = str_pad(mt_rand(0, 999999), 6, '0', STR_PAD_LEFT);
                $requestUser->otp = $otp;
                $requestUser->save();
                Mail::to($requestUser['email'])->send(new ForgetPassword($requestUser));
                return Response::json(['success'=>true,'message' => 'Please Check your email and reset password.']);
               
            } else {
                return Response::json(['success'=>false,'message' => 'Entered email is not exit in my record.']);
            }
        } catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
        }
    }


     /**
     *
     *
     *
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
       
        try {
            $user = User::where('otp', $request->otp)->first();
            if ($user) {
                $updatePassword = User::findOrFail($user->id);
                $updatePassword->password = Hash::make(trim($request->password));
                $updatePassword->otp = '';
                $updatePassword->save();
                return Response::json(
                    [
                        'success' => true,
                        'message' => 'Password has been changed successfully.'
                    ]
                );
            } else {
                return Response::json(
                    [
                        'success' => false,
                        'message' => 'Please enter correct OTP.'
                    ]
                );
            }
        } catch(Exception $ex){
            return Response::json(['success'=>false,'message' => $ex->getMessage()]);
            print_r($ex->getMessage());die;
        }
    }
	

     
 
}
