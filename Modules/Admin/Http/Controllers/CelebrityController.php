<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use App\Mail\ForgetPassword;
use Response;
use App\User;
use DataTables;
use Validator;
use Redirect;
use Mail;
use App\Models\CelebrityImage;
use App\Models\Celebrity;
use App\Http\Requests\Admin\AddCelebrityRequest;
use App\Models\CelebrityVideo;

class CelebrityController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $page = ($request->page) ? $request->page : 1;
        $celebrity = Celebrity::with('celebrityImage')->orderBy('id', 'desc');
        if (!empty($request->search)) {
            $search = $request->search;
            $celebrity->where('celebname', 'like', '%' . $search . '%');
        }
        $celebrity = $celebrity->paginate(6);
        return view('admin::celebrity.index', ['celebrity' => $celebrity, 'page' => $page]);
    }


    public function celebrityList(Request $request)
    {

        $celebrity = Celebrity::with('celebrityImage');
        $celebrity = $celebrity->paginate(10);
        $html = \View::make('admin::celebrity._celebrity-list', ['celebrity' => $celebrity])->render();
        return Response::json(['success' => true, 'html' => $html]);
    }

    public function getAddForm(Request $request)
    {
        return view('admin::celebrity.add-celebrity');
    }

    public function addCelebrity(Request $request)
    {
        try {
            $post = $request->all();
            $dir = url('public/images/profile');
            if (is_dir($dir) === false) {
                mkdir($dir);
            }
            if ($request->hasFile('profile')) {
                $image = $request->profile;
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images/profile/');
                $image->move($destinationPath, $name);
                $post['profile_pic'] = $name;
                $post['profile_type'] = 'local';
            }
            $celebrity = new Celebrity();
            $celebrity->create($post);
            return Response::json(['success' => true, 'message' => 'Celebrity Added Successfully.']);
        } catch (\Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }

    public function updateCelebrity(Request $request)
    {
       
        try {
            $celebrity =  Celebrity::where('id', $request->id)->first();
            if ($request->hasFile('profile')) {
                $image = $request->profile;
                $name = time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images/profile/');
                $image->move($destinationPath, $name);
                $celebrity->profile_pic = $name;
                $celebrity->profile_type = 'local';
                
            }
            if (!empty($celebrity)) {
                $celebrity->celebrity_type = $request->celebrity_type;
                $celebrity->celebname = $request->celebname;
                $celebrity->biography = $request->biography;
                $celebrity->birthday = $request->birthday;
                $celebrity->place_of_birth = $request->place_of_birth;
                $celebrity->known_for_department = $request->known_for_department;
                $celebrity->known_as = $request->known_as;
                $celebrity->gender = $request->gender;
                $celebrity->save();
                return Response::json(['success' => true, 'message' => 'Celebrity Updated Successfully.']);
            }
        } catch (\Exception $ex) {
            return Response::json(['success' => false, 'message' => $ex->getMessage()]);
        }
    }
    public function celebrityView($id)
    {
        $celebrity =  Celebrity::where('id', base64_decode($id))->first();
        return view('admin::celebrity.view_celebrity', ['celebrity' => $celebrity]);
    }

    public function deleteCelebrity($id)
    {
        $celebrity =  Celebrity::where('id', $id)->delete();
        return Response::json(['success' => true, 'message' => 'Celebrity Deleted Successfully.']);
    }

    public function getEditForm($id)
    {
        $celebrity =  Celebrity::where('id', base64_decode($id))->first();
        // echo "<pre>";
        // print_r($celebrity);die;
        return view('admin::celebrity.edit-celebrity', ['celebrity' => $celebrity]);
    }

    public function celebrityContentList(Request $request)
    {
        return view('admin::content.add-content');
    }

    public function getCelebrityList()
    {
        $celebrity =   Celebrity::all();
        $list = [];
        $list[] = "<option value=''>Select Celebrity</option>";
        foreach ($celebrity as $celeb) {
            $list[] = "<option value='" . $celeb->id . "'>" . $celeb->celebname . "</option>";
        }
        return Response::json(['success' => true, 'list' => $list]);
    }

    public function addContent(Request $request)
    {

        try {
            $dir = url('public/video');
            if (is_dir($dir) === false) {
                mkdir($dir);
            }
            $celeb =  Celebrity::where('id', $request->celeb_id)->first();
            $celeb->content = $request->content;
            $celeb->save();
            $images = $request->celeb_image;
            foreach ($images as $key => $image) {
                $image = $image;
                $name = $key . time() . '.' . $image->getClientOriginalExtension();
                $destinationPath = public_path('/images/profile/');
                $image->move($destinationPath, $name);
                $celebImage = new CelebrityImage();
                $celebImage->celeb_id = $celeb->id;
                $celebImage->image = $name;
                $celebImage->save();
            }

            $video = $request->celeb_video;
            // print_r($request->all());die;
            $name = $key . time() . '.' . $video->getClientOriginalName();
            $destinationPath = public_path('/video/');
            $video->move($destinationPath, $name);
            $celebVideo = new CelebrityVideo();
            $celebVideo->celeb_id = $celeb->id;
            $celebVideo->video = $name;
            $celebVideo->save();
            return Response::json(['success' => true, 'message' => 'Celebrity Content Added Successfully.']);
        } catch (\Exception $ex) {
            print_r($ex->getMessage());
            die;
        }
    }
}
